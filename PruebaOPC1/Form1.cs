﻿using System;
using System.Windows.Forms;
using System.Configuration;

using Kepware.ClientAce.OpcDaClient;
using Kepware.ClientAce.OpcCmn;


namespace PruebaOPC1
{
    public partial class Form1 : Form
    {
        DaServerMgt daServerMgt = new DaServerMgt();
        ServerIdentifier[] availableOPCServers;
        ListBox AvailableOPCServerList = new ListBox();

        int activeServerSubscriptionHandle;
        int activeClientSubscriptionHandle;

        String[] OPCItemNameStringArray = new String[2];
        String[] OPCItemValueStringArray = new String[2];
        String[] OPCItemQualityStringArray = new String[2];
        TextBox[] OPCItemTagStringArray = new TextBox[2];
        TextBox[] OPCItemTagQualityStringArray = new TextBox[2];

        String StrConexion = ConfigurationManager.ConnectionStrings["SQLConexion"].ConnectionString;
        int iSelectedRow = -1;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            int iTag1 = 0;
            int iTag2 = 0;
            DateTime dtCreateAt;
            DateTime dtTagTime = DateTime.Now;

            ConexionSQL con = new ConexionSQL();
            try
            {
                Int32.TryParse(txtTag1.Text, out iTag1);
                Int32.TryParse(txtTag2.Text, out iTag2);
                dtCreateAt = DateTime.Now;
                

                con.Sp_InsertarDatos(iTag1, iTag2, dtCreateAt, ref dtTagTime);

                lblMessage.Text = "Insercion Exitosa: "+ dtTagTime.ToString();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
            
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            ConexionSQL con = new ConexionSQL();
            try
            {
                dgvDatos.DataSource = con.Sp_ObtenerDatos();
                
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void dgvDatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            iSelectedRow = e.RowIndex;
            DataGridViewRow selectedRow = dgvDatos.Rows[iSelectedRow];
            txtTag1.Text = selectedRow.Cells[1].Value.ToString();
            txtTag2.Text = selectedRow.Cells[2].Value.ToString();


        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            ConexionSQL con = new ConexionSQL();
            if (iSelectedRow > 0)
            {
                DataGridViewRow selectedRow = dgvDatos.Rows[iSelectedRow];
                int iDevice1_Id = (int) selectedRow.Cells[0].Value;

                con.Eliminar(iDevice1_Id);
            }

        }

        

        private void btnEditar_Click(object sender, EventArgs e)
        {
            int iTag1;
            int iTag2;
            DateTime dtCreateAt;
            

            ConexionSQL con = new ConexionSQL();
            try
            {
                Int32.TryParse(txtTag1.Text, out iTag1);
                Int32.TryParse(txtTag2.Text, out iTag2);
                dtCreateAt = DateTime.Now;

                DataGridViewRow selectedRow = dgvDatos.Rows[iSelectedRow];
                int iDevice1_Id = (int)selectedRow.Cells[0].Value;

                con.Sp_UpdateTags(iDevice1_Id, iTag1, iTag2, dtCreateAt);

                lblMessage.Text = "Actualizacion Exitosa";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OPCItemTagStringArray[0] = txtTag1;
            OPCItemTagStringArray[1] = txtTag2;

            OPCItemTagQualityStringArray[0] = txtQuality0;
            OPCItemTagQualityStringArray[1] = txtQuality1;

            OPCItemNameStringArray[0] = "Channel1.Device1.Tag1";
            OPCItemNameStringArray[1] = "Channel1.Device1.Tag2";

            ListOPCServers();
            ConnectToServer();
            SubscribeToOPCDAServerEvents();
            SubscribirItems();


        }

        private void ListOPCServers()
        {
            OpcServerEnum opcServerEnum = new OpcServerEnum();
            String nodeName = "localhost";
            bool returnAllServers = false;
            ServerCategory[] serverCatagories = { ServerCategory.OPCDA };

            try
            {
                opcServerEnum.EnumComServer(nodeName, returnAllServers, serverCatagories, out availableOPCServers);
                AvailableOPCServerList.Items.Clear();


                if (availableOPCServers.GetLength(0) > 0)
                {
                    foreach (ServerIdentifier opcServer in availableOPCServers)
                    {
                        AvailableOPCServerList.Items.Add(opcServer.ProgID);
                    }

                    AvailableOPCServerList.Enabled = true;
                }
                else
                {
                    MessageBox.Show("No OPC servers found at node: " + nodeName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Handled List OPC Servers exception. Reason: " + ex.Message);
            }


        }

        private void ConnectToServer()
        {

            String url = "";
            int selectedServerIndex = 0;
            foreach (String Item in AvailableOPCServerList.Items)
            {

                if (Item.Contains("KEPServerEX"))
                {
                    url = availableOPCServers[selectedServerIndex].Url;
                    break;
                }
                selectedServerIndex++;
            }

            int clientHandle = 1;
            ConnectInfo connectInfo = new ConnectInfo();
            connectInfo.LocalId = "en";
            connectInfo.KeepAliveTime = 60000;
            connectInfo.RetryAfterConnectionError = true;
            connectInfo.RetryInitialConnection = false;
            bool connectFailed = false;


            try
            {
                daServerMgt.Connect(url, clientHandle, ref connectInfo, out connectFailed);
                lblMessage.Text = "Conexion:Success";
            }
            catch (Exception ex)
            {
                connectFailed = true;
                lblMessage.Text = "Conexion:Fail";
            }

        }

        private void SubscribeToOPCDAServerEvents()
        {
            daServerMgt.DataChanged += new DaServerMgt.DataChangedEventHandler(daServerMgt_DataChanged);
        }

        public void daServerMgt_DataChanged(int clientSubscription, bool allQualitiesGood, bool noErrors, ItemValueCallback[] itemValues)
        {


            // We need to forward the callback to the main thread of the application if we access the GUI directly from the callback. 
            //It is recommended to do this even if the application is running in the back ground.
            object[] DCevHndlrArray = new object[4];
            DCevHndlrArray[0] = clientSubscription;
            DCevHndlrArray[1] = allQualitiesGood;
            DCevHndlrArray[2] = noErrors;
            DCevHndlrArray[3] = itemValues;
            BeginInvoke(new DaServerMgt.DataChangedEventHandler(DataChanged), DCevHndlrArray);

        }



        public void DataChanged(int clientSubscription, bool allQualitiesGood, bool noErrors, ItemValueCallback[] itemValues)
        {


            try
            {

                if (activeClientSubscriptionHandle == clientSubscription)
                {

                    foreach (ItemValueCallback itemValue in itemValues)
                    {

                        int itemIndex = (int)itemValue.ClientHandle;


                        if (itemValue.Value == null)
                        {
                            OPCItemTagStringArray[itemIndex].Text = "Unknown";
                            OPCItemValueStringArray[itemIndex] = "Unknown";
                        }
                        else
                        {
                            OPCItemTagStringArray[itemIndex].Text = itemValue.Value.ToString();
                            OPCItemValueStringArray[itemIndex] = itemValue.Value.ToString();
                        }

                        // Update quality control:
                        OPCItemTagQualityStringArray[itemIndex].Text = itemValue.Quality.Name;
                        OPCItemQualityStringArray[itemIndex] = itemValue.Quality.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                //~~ Handle any exception errors here.
                MessageBox.Show("Handled Data Changed exception. Reason: " + ex.Message);
            }

        }

        public void SubscribirItems()
        {
            int itemIndex;

            int clientSubscriptionHandle = 1;
            bool active = true;
            int updateRate = 1000;

            Single deadBand = 0;
            ItemIdentifier[] itemIdentifiers = new ItemIdentifier[2];

            for (itemIndex = 0; itemIndex <= 1; itemIndex++)
            {
                itemIdentifiers[itemIndex] = new ItemIdentifier();
                itemIdentifiers[itemIndex].ItemName = OPCItemNameStringArray[itemIndex];
                itemIdentifiers[itemIndex].ClientHandle = itemIndex;
                itemIdentifiers[itemIndex].DataType = Type.GetType("System.Int16");
            }

            int revisedUpdateRate;

            try
            {
                daServerMgt.Subscribe(clientSubscriptionHandle, active, updateRate, out revisedUpdateRate, deadBand,
                                        ref itemIdentifiers, out activeServerSubscriptionHandle);

                activeClientSubscriptionHandle = clientSubscriptionHandle;

                for (itemIndex = 0; itemIndex <= 1; itemIndex++)
                {
                    if (itemIdentifiers[itemIndex].ResultID.Succeeded == false)
                    {
                        MessageBox.Show("Failed to add item " + itemIdentifiers[itemIndex].ItemName + " to subscription");
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Handled Subscribe exception. Reason: " + ex.Message);
            }


        }
    }
}
